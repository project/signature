<?php

namespace Drupal\Tests\signature\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the module re-install.
 *
 * @group signature
 */
class SignatureUninstallTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that the module could be re-installed.
   */
  public function testReinstall() {
    \Drupal::service('module_installer')->install(['signature']);
    \Drupal::service('module_installer')->uninstall(['signature']);
    \Drupal::service('module_installer')->install(['signature']);
  }

}
